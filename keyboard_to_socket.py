# Python 3.8

import threading
import time
import keyboard
import socket

key_left = False
key_right = False
key_up = False
key_down = False
key_shift= False
key_rightshift = False
key_ctrl = False

up_down = 0
left_right = 0
rock_n_roll = 0
motor_status = False
follow_mode = True

command = b'off/course/0/0/0/0'

#ADDR = '127.0.0.1'
#ADDR = '192.168.0.96'   # jetson tx1
#ADDR = '192.168.0.99'   # Alex UBUNTU
ADDR = '192.168.0.103' # jetson tx2
#ADDR = '192.168.0.93'  # Serg
PORT = 5100


class SBGC_TCP_client:
    # класс запускает TCP клиента на передачу команд управления и прием состояний гироподвеса

    def __init__(self):

        self._do = False
        # переменные канала связи
        self._connected  = False
        self._socket = None
        self.n_beat = 0

        self._last_command = b'off/course/0/0/0/0:'

    def start(self, IP_ADDR, PORT, keep_alive_intrval):

        self._interval = keep_alive_intrval
        self.socket_addr = (IP_ADDR, PORT)

        self._do = True

        # watchdog по приему
        self._receiver_lost = False  # флаг потери приема данных от клиента
        self._R_watchdog = threading.Timer(self._interval * 2, self._R_alarm)
        self._R_watchdog.start()

        # watchdog по передаче
        self._transmitted = False  # флаг наличия передачи пакета
        self._T_watchdog = threading.Timer(self._interval, self._T_alarm)
        self._T_watchdog.start()

        # запускаем основной цикл поддержания соединения
        self._doing(IP_ADDR, PORT)

    def close(self):
        self._do = False

    def _T_watchdog_reset(self):
        # перезапуск watchdog таймера передачи
        if self._do:
            if self._T_watchdog is time:
                self._T_watchdog.cancel()
                self._T_watchdog = None
            self._transmitted = True
            self._T_watchdog = threading.Timer(self._interval, self._T_alarm)
            self._T_watchdog.start()

    def _T_alarm(self):
        if self._T_watchdog is time:
            self._T_watchdog.cancel()
        # выдаем heart_beat в сторону сервера
        if self._connected:
            self.send(self._last_command)

        self._transmitted = False
        if self._do:
            self._T_watchdog = threading.Timer(self._interval, self._T_alarm)
            self._T_watchdog.start()

    def _R_watchdog_reset(self):
        # перезапуск watchdog таймера приема
        if self._do:
            if self._R_watchdog is not None:
                self._R_watchdog.cancel()
            self._receiver_lost = False
            self._R_watchdog = threading.Timer(self._interval * 2, self._R_alarm)
            self._R_watchdog.start()

    def _R_alarm(self):
        # не дождались приема данных от сервера
        if self._R_watchdog is not None:
            self._R_watchdog.cancel()
        print('Нет данных от сервера!')                                                                          # debug
        self._receiver_lost = True

        #TODO
        # отправка данных о статусе канала связи для отображения в интерфейсе НСУ

        if self._do:
            self._R_watchdog = threading.Timer(self._interval * 2, self._R_alarm)
            self._R_watchdog.start()

    def send(self, tr_data):
        # отправка данных на сервер (коптер)
        if self._socket is None:
            return False
        try:
            if self._receiver_lost:
                to_transmit = tr_data + b'R_LOST:'
            else:
                to_transmit = tr_data + b'R_OK:'

            self._socket.sendall(to_transmit)
            self._last_command = tr_data

            return True

        except socket.error as err:
            print('Ошибка соединения', err)                                                                     # debug
            return False

    def _doing(self, IP_ADDR, PORT):
        # основной цикл приема данных с сервера (статус канала и подвеса)
        while self._do:
            try:
                self._socket = socket.socket()
                self._socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            except socket.error as err:
                print('Ошибка открытия сокета', err)                                                            # debug

            self._connected = False

            while self._do:
                if not self._connected:
                    try:
                        self._socket.connect((IP_ADDR, PORT))
                        self._connected = True
                    except socket.error as err:
                        self._connected = False
                        #print('Ошибка соединения', err)                                                         # debug
                        break
                else:
                    try:
                        data = self._socket.recv(128)

                        # сброс таймера по приему данных от сервера
                        self._R_watchdog_reset()

                        # вывод данных, принятых от сервера
                        # в оболочке - вывод данных поверх видео и в надписи и транспаранты
                        print('D:',data)                                                                        # debug

                    except socket.error as err:
                        self._connected = False
                        print('По приему', err)
                        break
        if not self._do:
            return


def pressed_key(key):
    # клавиши
    global client

    global key_left
    global key_right
    global key_up
    global key_down
    global key_shift
    global key_rightshift
    global key_ctrl

    global up_down
    global left_right
    global rock_n_roll
    global follow_mode
    global motor_status

    global command

    speed = 60

    last_command = command

    if key.name == 'esc':
        if key.event_type == 'down':

            client.close()
            keyboard.unhook_all()

    if key.name == 'space':
        if key.event_type == 'down':
            if motor_status:
                if follow_mode:
                    follow_mode = False
                    command = b'on/azimuth/0/0/0/0:'
                else:
                    follow_mode= True
                    command = b'on/course/0/0/0/0:'
                #print(command)                                                                                  # debug
                client.send(command)

    if key.name == 'right shift':
        if key.event_type == 'down':
            key_rightshift = True
        else:
            key_rightshift = False

    if (key.name == 'shift'):
        if (key.event_type == 'down'):
            key_shift = True
        else:
            key_shift = False

    # зажато два шифта
    if key_shift and key_rightshift:
        if not motor_status:
            motor_status = True
            command = b'on/course/0/0/0/0:'
        else:
            motor_status = False
            command = b'off/course/0/0/0/0:'
        #print(command)                                                                                          # debug
        client.send(command)

    # управление поворотом имеет смысл только при включенных моторах
    if motor_status:
        fields = command.split(b'/')

        if (key.name == 'ctrl') or (key.name == 'right ctrl'):
            if key.event_type == 'down':
                key_ctrl = True
            else:
                key_ctrl = False

        control_button = False

        if key.name == 'up':
            control_button = True
            if key.event_type == 'down':
                if not key_up:
                    key_up = True
                    up_down = up_down + speed
            else:
                if key_up:
                    key_up = False
                    up_down = up_down - speed

        if key.name == 'down':
            control_button = True
            if key.event_type == 'down':
                if not key_down:
                    key_down = True
                    up_down = up_down - speed
            else:
                if key_down:
                    key_down = False
                    up_down = up_down + speed

        if key.name == 'left':
            control_button = True
            if key.event_type == 'down':
                if not key_left:
                    if key_ctrl:
                        key_left = True
                        rock_n_roll = rock_n_roll - speed
                    else:
                        key_left = True
                        left_right = left_right - speed
            else:
                if key_left:
                    if key_ctrl:
                        key_left = False
                        rock_n_roll = rock_n_roll + speed
                    else:
                        key_left = False
                        left_right = left_right + speed

        if key.name == 'right':
            control_button = True
            if key.event_type == 'down':
                if not key_right:
                    if key_ctrl:
                        key_right = True
                        left_right = 0
                        rock_n_roll = rock_n_roll + speed
                    else:
                        key_right = True
                        left_right = left_right + speed
                        rock_n_roll = 0
            else:
                if key_right:
                    if key_ctrl:
                        key_right = False
                        left_right = 0
                        rock_n_roll = rock_n_roll - speed
                    else:
                        key_right = False
                        left_right = left_right - speed
                        rock_n_roll = 0

        if control_button:
            if rock_n_roll == 0:
                fields[2] = b'0'
            elif rock_n_roll == -speed:
                fields[2] = b'-'
            elif rock_n_roll == speed:
                fields[2] = b'+'

            if up_down == 0:
                fields[3] = b'0'
            elif up_down == -speed:
                fields[3] = b'-'
            elif up_down == speed:
                fields[3] = b'+'

            if left_right == 0:
                fields[4] = b'0'
            elif left_right == -speed:
                fields[4] = b'-'
            elif left_right == speed:
                fields[4] = b'+'
            command = fields[0] + b'/' + fields[1] + b'/' + fields[2] + b'/' + fields[3] + b'/' + fields[4] + b'/' + \
                      fields[5]
            if last_command != command:
                #print(command)                                                                                  # debug
                client.send(command)

    #print(key.name, key.event_type)                                                                             # debug


if __name__ == '__main__':

    # создает сокет
    client = SBGC_TCP_client()

    # перехватываем клавиатуру
    keyboard.hook(pressed_key,True)

    # запускаем клиента
    client.start(ADDR, PORT, 1)