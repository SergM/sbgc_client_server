# Python 3.8
import time
import threading
import socket
from SBGC_control import Gyro_gimbal

ADDR = ''
PORT = 5100


class SBGC_TCP_server:

    # класс запускает TCP сервер на прием команд управления и передачу состояний гироподвеса
    def __init__(self, IP_ADDR, PORT, keep_alive_intrval):

        # переменные таймера
        self._interval = keep_alive_intrval
        self._done = False

        # переменные канала связи
        self.socket_addr = (IP_ADDR, PORT)
        self._connected  = False
        self._connection = None

        # переменные управления подвесом
        self._motor_status = b'off'
        self._follow_mode  = b'course'
        self._rock_n_roll  = b'0'
        self._up_down      = b'0'
        self._left_right   = b'0'
        self._speed = 20

        # запускаем связь с подвесом
        self.SBGC32 = Gyro_gimbal(freq=10)

        # watchdog по приему
        self._watchdog = threading.Timer(self._interval * 2, self._alarm)
        self._receiver_lost = False  # флаг потери приема данных от клиента

        # запускаем таймер отправки статуса сервера
        self._keep_alive_timer()

        # запускаем основной цикл поддержания соединения
        self._doing(IP_ADDR, PORT)

    def _watchdog_reset(self):
        # перезапуск watchdog таймера
        self._watchdog.cancel()
        self._receiver_lost = False
        self._watchdog = threading.Timer(self._interval * 2, self._alarm)
        self._watchdog.start()

    def _alarm(self):
        # не дождались приема данных от клиента
        self._receiver_lost = True

        #TODO
        # отправка данных системе управления коптером о потере связи в канале управления гироподвесом
        print('Нет данных от клиента')  # debug

        self._watchdog = threading.Timer(self._interval * 2, self._alarm)
        self._watchdog.start()

    def _keep_alive_timer(self):

        #print('Connected',self._connected)                                                                      # debug

        if self._connected:
            self._send_status()

        # если не установлен флаг _done, то выполняем всё заново
        if not self._done:
            threading.Timer(self._interval, self._keep_alive_timer).start()

    def _send_status(self):

        # получаем статус гироподвеса и добавляем Remote alarm
        status = self.SBGC32.get_status()

        if self._receiver_lost:
            to_transmit = status + ':R_LOST:'
        else:
            to_transmit = status + ':R_OK:'

        # отправляем статус клиенту
        try:
            self._transmitted = True
            self._connection.sendall(str.encode(to_transmit))
        except socket.error as err:
            #print('По передаче', err)                                                                           # debug
            self._connection.close()
            self._watchdog.cancel()
            self._connected = False

    def _doing(self, IP_ADDR, PORT):
        # основной цикл приема данных от клиента и передачи команд на подвес

        # запускаем сервер
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

            s.bind((IP_ADDR, PORT))
            s.listen()
            while True:
                if not self._connected:
                    self._connection, addr = s.accept()

                    print('Соединились с', addr)                                                                # debug
                    self._watchdog.cancel()
                    self._watchdog = threading.Timer(self._interval * 2, self._alarm)
                    self._watchdog.start()
                    self._connected = True
                    while True:

                        try:
                            data = self._connection.recv(32)
                            self._watchdog_reset()
                            if len(data)>0:
                                self._pars(data)

                        except socket.error as err:
                            print('По приему', err)                                                             # debug
                            self._connection.close()
                            self._watchdog.cancel()
                            self._connected = False
                            #TODO
                            # отправка данных системе управления коптером о потере связи в канале управления гироподвесом
                            break

                        if not data:
                            print('Разорвано соединение с', addr)                                               # debug
                            self._connection.close()
                            self._watchdog.cancel()
                            self._connected = False
                            #TODO
                            # отправка данных системе управления коптером о потере связи в канале управления гироподвесом
                            break

    def _pars(self, data):

        # команды имеют формат: 'поле1/поле2/поле3/поле4/поле5/резерв:'
        # поле1 - по pitch ('off', 'on')
        # поле2 - по pitch ('course', 'azimuth')
        # поле3 - по roll  ('-' - против часовой стрелки, '+' - по часовой стрелке, '0' - нет поворота)
        # поле4 - по pitch ('-' - вниз, '+' - вверх, '0' - нет поворота)
        # поле5 - по yaw   ('-' - влево, '+' - вправо, '0' - нет поворота)
        # резерв - '0'
        # print("data  ",data)
        data_list = data.split(b':')
        for command in data_list:
            if len(command)>0:

                print("command",command)                                                                                  # debug

                # разбиваем на название команды на поля
                fields = command.split(b'/')
                if len(fields)<6:
                    continue
                # проверяем изменения в переменных управления
                if fields[0] != self._motor_status:
                    self._motor_status = fields[0]
                    # надо менять статус мотора
                    if self._motor_status == b'off':
                        self.SBGC32.stop()
                    elif self._motor_status == b'on':
                        self.SBGC32.run()
                else:
                    # можно работать с друими праметрами
                    if fields[1] != self._follow_mode:
                        self._follow_mode = fields[1]
                        # надо менять режим удержания
                        if self._follow_mode == b'course':
                            self.SBGC32.follow(True)
                        elif self._follow_mode == b'azimuth':
                            self.SBGC32.follow(False)
                    else:
                        # и только, если не менялись режим улержания и питание моторов, можно менять скорости
                        # причем, можно не следить за тем, изменялась скорость или нет от последнего раза
                        if fields[2] == b'+':
                            self._rock_n_roll = self._speed
                        elif fields[2] == b'-':
                            self._rock_n_roll = -self._speed
                        else:
                            self._rock_n_roll = 0

                        if fields[3] == b'+':
                            self._up_down = self._speed
                        elif fields[3] == b'-':
                            self._up_down = -self._speed
                        else:
                            self._up_down = 0

                        if fields[4] == b'+':
                            self._left_right = self._speed
                        elif fields[4] == b'-':
                            self._left_right = -self._speed
                        else:
                            self._left_right = 0
                        print(self._left_right, self._up_down, self._rock_n_roll)
                        self.SBGC32.turn(self._left_right, self._up_down, self._rock_n_roll)


if __name__ == '__main__':
    # запускаем сервер
    server = SBGC_TCP_server(ADDR, PORT, 1)
