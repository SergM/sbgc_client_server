# Python 3.8

import threading
import sys
import time
import glob
import serial
import termios

CMD_Preamble = 0x3E  # для версии протокола №1

CMD_BOARD_INFO = 86  # информация о плате
CMD_READ_PARAMS_3 = 21  # параметры работы устройства
CMD_MOTORS_ON = 77  # включение моторов
CMD_MOTORS_OFF = 109  # отключение моторов
CMD_CONTROL_CONFIG = 90
CMD_CONTROL = 67  # тут есть управление двигателями
# для включения фиксации по заданным осям MODE_NO_CONTROL=0
# для задания скорости поворота по осям MODE_SPEED=1
# для задания угла поворота по осям MODE_ANGLE=2
CMD_REALTIME_DATA_3 = 23  # тут есть пропуск шагов двигателем в SYSTEM_ERROR, BAT_LEVEL и MOTOR_POWER[3]
CMD_SET_ADJ_VARS_VAL = 31  # для включения FOLLOW_MODE YAW

CMD_CONFIRM = 67  # для получения подтверждения команд
CMD_GET_ANGLES = 73  # текущие углы подвеса
CMD_GET_ANGLES_EXT = 61


class Gyro_gimbal:
    def __init__(self, freq):

        self.error_status = 'CLOSED'  # CLOSED
        # OK
        # Board searching
        # No response from board
        # Stopped

        self._waiting_for_response = False

        # переменные таймера
        self.thread_status = False  # True, если всё выполнилось за последние 100 итераций
        self._thread_cnt = 0
        self._next_t = time.time()
        self._increment = 1.0 / freq
        self._done = False

        self._follow_mode = False
        self._follow_on = False
        self._follow_off = False

        # скорости моторов
        self._roll_speed = 0.0
        self._pitch_speed = 0.0
        self._yaw_speed = 0.0
        self._speed_changed = False

        # углы по осям
        self._roll_ang = 0.0
        self._pitch_ang = 0.0
        self._yaw_ang = 0.0
        self._angels_changed = True

        # триггеры включения моторов
        self.motors_are_on = False
        self._switch_on_motors = False
        self._switch_off_motors = False

        self._roll_IMU_ang = 0
        self._pitch_IMU_ang = 0
        self._yaw_IMU_ang = 0
        self._yaw_diff_ang = 0
        self._miss_steps_error = 0

        self._ser_port = ''
        self._active_serial = serial
        self._first_comm_start = True

        # сразу запускаем работу с платой
        self._doing()

    def _doing(self):
        # итеративное выполнение через заданный интервал
        self._next_t = time.time()

        ################################################################################################################
        # выполнение всех операций с гироподвесом тут

        if self._ser_port == '':
            # если ещё не нашли порт с SimpleGBC32, то ищем
            self.error_status = 'Board searching'
            print("...поиск платы управления")
            self._ser_port = self._lookup_SBGC32(self)
            #print("Serial port: ", self._ser_port)
            if self._ser_port != '':
                # открываю порт, с которым потом и буду работать
                try:
                    self._active_serial = serial.Serial(self._ser_port, baudrate=115200)
                    self.error_status = 'OK'
                    self._first_comm_start = True
                except serial.SerialException:
                    print("Serial exception")
                    self._ser_port = ''

        else:
            # если есть активный порт, то...

            # если только включились, то надо обновить все команды изменения статусов и остановить все вращения
            if self._first_comm_start:

                if self._follow_mode:
                    self._follow_on = True
                else:
                    self._follow_off = True

                # скорости моторов
                self._roll_speed = 0.0
                self._pitch_speed = 0.0
                self._yaw_speed = 0.0
                self._speed_changed = True

                # триггеры включения моторов
                if self.motors_are_on:
                    self._switch_on_motors = True
                else:
                    self._switch_off_motors = True

                self._first_comm_start = False

            if self.motors_are_on:
                if self._speed_changed:
                    # если есть изменение скорости моторов,
                    # то надо слать команды управления

                    status = self._check_status()
                    if status == 0:
                        roll_mode = 0x01 if (self._roll_speed != 0) else 0x00
                        pitch_mode = 0x01 if (self._pitch_speed != 0) else 0x00
                        yaw_mode = 0x01 if (self._yaw_speed != 0) else 0x00

                        roll_speed_2s = self._int_to_2s(int(self._roll_speed / 0.1220740379))
                        pitch_speed_2s = self._int_to_2s(int(self._pitch_speed / 0.1220740379))
                        yaw_speed_2s = self._int_to_2s(int(self._yaw_speed / 0.1220740379))

                        roll_ang_2s = self._int_to_2s(0)
                        pitch_ang_2s = self._int_to_2s(0)
                        yaw_ang_2s = self._int_to_2s(0)

                        self._speed_changed = False

                        self._send_command(self._active_serial, CMD_CONTROL, [roll_mode, pitch_mode, yaw_mode] +
                                           roll_speed_2s + roll_ang_2s +
                                           pitch_speed_2s + pitch_ang_2s +
                                           yaw_speed_2s + yaw_ang_2s)
                        response = self._get_response(self._active_serial, 0.1)

                        if (response[-1]) == 0 and (response[1] == CMD_CONFIRM) and (response[4] == CMD_CONTROL):
                            self.error_status = 'OK'
                        else:
                            self.error_status = 'No response from board'

                elif self._angels_changed:

                    roll_ang_2s = self._int_to_2s(int(self._roll_ang / 0.02197265625))
                    pitch_ang_2s = self._int_to_2s(int(self._pitch_ang / 0.02197265625))
                    yaw_ang_2s = self._int_to_2s(int(self._yaw_ang / 0.02197265625))
                    self._send_command(self._active_serial, CMD_CONTROL, [0x02, 0x02, 0x02] +
                                       [0, 0] + roll_ang_2s + [0, 0] + pitch_ang_2s + [0, 0] + yaw_ang_2s)

                    # до 0,1 секунд ждем в
                    response = self._get_response(self._active_serial, 0.1)

                    if (response[-1]) == 0 and (response[1] == CMD_CONFIRM) and (response[4] == CMD_CONTROL):
                        self.error_status = 'OK'
                    else:
                        self.error_status = 'No response from board'

                    self._angels_changed = False

                elif self._follow_on:
                    a = 1
                    self._send_command(self._active_serial, CMD_SET_ADJ_VARS_VAL,
                                       [1] +
                                       [31] +
                                       list(a.to_bytes(4, byteorder='little', signed=True)))
                    response = self._get_response(self._active_serial, 0.1)
                    # print(response)
                    if (response[-1]) == 0 and (response[1] == CMD_CONFIRM) and (response[4] == CMD_SET_ADJ_VARS_VAL):
                        self.error_status = 'OK'
                        self._follow_mode = True
                    else:
                        self.error_status = 'No response from board'
                    self._follow_on = False

                elif self._follow_off:
                    a = 0
                    self._send_command(self._active_serial, CMD_SET_ADJ_VARS_VAL,
                                       [1] +
                                       [31] +
                                       list(a.to_bytes(4, byteorder='little', signed=True)))
                    response = self._get_response(self._active_serial, 0.1)
                    # print(response)
                    if (response[-1]) == 0 and (response[1] == CMD_CONFIRM) and (response[4] == CMD_SET_ADJ_VARS_VAL):
                        self.error_status = 'OK'
                        self._follow_mode = False
                    else:
                        self.error_status = 'No response from board'
                    self._follow_off = False

                else:
                    # если не надо слать команд управления, то запрашиваем различные параметры
                    # и обновляем свойства класса
                    if not self._waiting_for_response:

                        status = self._check_status()

                        if status == 0:
                            self.error_status = 'OK'

                        else:
                            self.error_status = 'No response from board'
                            # print('Не принята строка состояния')

            if self._switch_on_motors:
                # если надо включить моторы, то посылаем команду на включение
                # print("Включаем моторы")
                self._send_command(self._active_serial, CMD_MOTORS_ON)
                # ждем до 0.5с подтверждения включения
                response = self._get_response(self._active_serial, 0.5)

                if (response[-1]) == 0 and (response[1] == CMD_CONFIRM) and (response[4] == CMD_MOTORS_ON) \
                        and (response[5] == CMD_MOTORS_ON):

                    self.error_status = 'OK'
                    # print("Моторы включились")
                    # print()

                    self._switch_on_motors = False
                    self.motors_are_on = True

                else:
                    self.error_status = 'No response from board'
                    # print("Нет подтверждения включения мторов")
                    # print()

            if self._switch_off_motors:
                # если надо включить моторы, то посылаем команду на включение
                # print("Выключаем моторы")
                self._send_command(self._active_serial, CMD_MOTORS_OFF, [0])
                # ждем до 0.5с подтверждения включения
                response = self._get_response(self._active_serial, 0.5)
                # print(response)
                if (response[-1]) == 0 and (response[1] == CMD_CONFIRM) and (response[4] == CMD_MOTORS_OFF):
                    self.error_status = 'OK'
                    # print("Моторы выключились")
                    # print()
                    self._switch_off_motors = False
                    self.motors_are_on = False

                else:
                    self.error_status = 'No response from board'
                    # print("Нет подтверждения отключения мторов")

        if self._done:
            self.error_status = 'Stopped'
            pass

        # конец операций с гироподвесом
        ################################################################################################################

        # вычисляем следующий интервал для таймера
        self._next_t += self._increment
        # укорачиваем интервал ожидания на время, потраченное на выполнения операций
        interval = self._next_t - time.time()
        if interval < 0:
            interval = 0
            self.thread_status = False
            self._thread_cnt = 0
            # print('не хватило интервала')
        else:
            self._thread_cnt += 1
            if self._thread_cnt >= 100:
                self._thread_cnt = 100
                self.thread_status = True

        if not self._done:
            # если не установлен флаг _done, то выполняем всё заново
            threading.Timer(interval, self._doing).start()

    def get_status(self):

        if self._ser_port == '':
            return self.error_status

        if self.motors_are_on:
            field_0 = 'on/'
        else:
            field_0 = 'off/'

        if self._follow_mode:
            field_1 = 'course/'
        else:
            field_1 = 'azimuth/'

        field_2 = format(self._roll_IMU_ang, '.2f') + '/'
        field_3 = format(self._pitch_IMU_ang, '.2f') + '/'
        field_4 = format(self._yaw_IMU_ang, '.2f') + '/'

        field_5 = format(self._yaw_diff_ang, '.2f') + '/'

        field_6 = format(self._miss_steps_error, 'd') + '/'

        status_str = 'motor=' + field_0 +\
                     'follow=' + field_1 +\
                     'roll=' + field_2 +\
                     'pitch=' + field_3 +\
                     'yaw=' + field_4 +\
                     'yaw_diff=' + field_5 +\
                     'miss_steps=' + field_6 + self.error_status

        return status_str

    def _check_status(self):
        self._send_command(self._active_serial, CMD_REALTIME_DATA_3)
        response = self._get_response(self._active_serial, 0.05)

        if (response[-1]) == 0 and (response[1] == CMD_REALTIME_DATA_3):

            # разбираем ответ. выбираем только то, что надо

            self._miss_steps_error = ((response[18] + response[19] << 8) & (1 << 9)) >> 9

            self._roll_IMU_ang = 0.02197265625 * self._i2s_to_int(response[36:38])
            self._pitch_IMU_ang = 0.02197265625 * self._i2s_to_int(response[38:40])
            self._yaw_IMU_ang = 0.02197265625 * self._i2s_to_int(response[40:42])

            self._yaw_frame_IMU_ang = 0.02197265625 * self._i2s_to_int(response[46:48])

            # вычисляю разницу между подвесом и рамой
            self._yaw_diff_ang = (self._yaw_IMU_ang - self._yaw_frame_IMU_ang) % 360

            # привожу к формату (-180 ... +180]
            if self._yaw_diff_ang > 180:
                self._yaw_diff_ang = self._yaw_diff_ang - 360
            elif self._yaw_diff_ang <= -180:
                self._yaw_diff_ang = self._yaw_diff_ang + 360

            # self._roll_target_ang = 0.02197265625 * self._i2s_to_int(response[48:50])
            # self._pitch_target_ang = 0.02197265625 * self._i2s_to_int(response[50:52])
            # self._yaw_target_ang = 0.02197265625 * self._i2s_to_int(response[52:54])

            self._roll_motor_power = response[64]
            self._pitch_motor_power = response[65]
            self._yaw_motor_power = response[66]

            #                                                                                                      debug
            # print('Состояние: IMU_ang = [ ' + format(self._roll_IMU_ang, '.2f') + '; '
            #         + format(self._pitch_IMU_ang, '.2f') + '; ' + format(self._yaw_IMU_ang, '.2f') + ' ]'
            #         + '\tyaw_diff_ang = [' + format(self._yaw_diff_ang, '.2f') + '];'
            #         + '\tmiss_steps_error =' + str(self._miss_steps_error)
            # + '\tmotors_power = [ ' + str(self._roll_motor_power) + '; ' + str(self._pitch_motor_power) + '; ' + str(
            #     self._yaw_motor_power) + ' ]')

            self.error_status = 'OK'
            return 0
        else:
            self.error_status = 'No response from board'
        return 1

    def set_angels(self, roll=0, pitch=0, yaw=0):
        # установка углов по осям

        self._angels_changed = True

        self._roll_ang = roll
        self._pitch_ang = pitch
        self._yaw_ang = yaw

    def follow(self, mode):
        # изменение скорости поворотов
        # leftright - скорость изменения угла по YAW
        # updown - скорость изменения угла по PITCH
        # roll - скорость изменения угла по ROLL
        # print(mode)
        if mode:
            self._follow_on = True
        else:
            self._follow_off = True

    def turn(self, leftright=0.0, updown=0.0, roll=0.0):
        # изменение скорости поворотов
        # leftright - скорость изменения угла по YAW
        # updown - скорость изменения угла по PITCH
        # roll - скорость изменения угла по ROLL
        self._speed_changed = True

        self._roll_speed = roll
        self._pitch_speed = updown
        self._yaw_speed = leftright

    def run(self):
        # запуск итеративного процесса управления и считывания информации
        self._done = False
        self._roll_speed = 0.0
        self._pitch_speed = 0.0
        self._yaw_speed = 0.0
        self._speed_changed = True
        self._switch_on_motors = True
        self._waiting_for_response = False

    def stop(self):
        # останавливаем все двигатели
        self._roll_speed = 0.0
        self._pitch_speed = 0.0
        self._yaw_speed = 0.0
        self._speed_changed = True
        self._switch_off_motors = True
        self._waiting_for_response = False

    def close(self):

        # останавливаем все двигатели
        self._roll_speed = 0.0
        self._pitch_speed = 0.0
        self._yaw_speed = 0.0
        self._speed_changed = True
        self._switch_off_motors = True
        self._waiting_for_response = False

        # отсанавливаем итеративное выполнение
        self._done = True

    def get_motors_status(self):
        # возвращаем статус включенности мотора
        return self.motors_are_on

    def _send_command(self, ser, cmd_id=CMD_BOARD_INFO, cmd_payload=[0]):
        # отправка данных на плату управления
        if self._waiting_for_response:
            # если ожидается получение данных от платы управления, то нельзя посылать команду
            return

        cmd_payload_size = len(cmd_payload)
        query = [CMD_Preamble, cmd_id, cmd_payload_size, (cmd_id + cmd_payload_size) % 256] + cmd_payload + \
                [sum(cmd_payload) % 256]

        try:
            # перед выдачей данных удаляем свё из приёмного буфера
            self._waiting_for_response = True
            ser.flushInput()
            ser.write(query)
        except (serial.SerialException, AttributeError, OSError, termios.error):
            self.error_status = 'No response from board'
            self._ser_port = ''

    def _get_response(self, ser, timeout=0.1):
        # получение ответа от платы управления
        # input:
        #   ser - объект открытого con-порта
        #   timeout - таймаут в секундах
        # output
        #   список байт (int), полученных от платы управления + один байт статуса
        #   статусы:
        #       0 - ОК
        #       1 - вышел таймаут
        #       2 - не верный первый байт ответа
        #       3 - не верная контрольная сумма поля команды
        #       4 - не верная контрольная сумма поля данных
        #       5 - строка длиннее чем указано в поле длины
        #       6 - потерян com-порт

        string = []
        flag = [0]

        tic = time.time()
        while True:
            # ждем пока не получим валидную строку или не выйдет таймаут
            try:
                bytes_in_buffer = ser.in_waiting
            except (serial.SerialException, AttributeError, OSError):
                self._ser_port = ''
                flag = [6]
                break

            if bytes_in_buffer > 0:
                # набираем строку
                try:
                    string = string + list(ser.read(ser.in_waiting))
                except serial.SerialException:
                    self._ser_port = ''
                    flag = [6]
                    break

                if len(string) >= 4:
                    # если не сошлась контрольная сумма команды или не верный первый символ,
                    # то можно прерывать прием и сигнализировать об ошибке
                    if string[0] != 0x3E:
                        flag = [2]
                        break
                    elif not ((string[0] == 0x3E) and ((string[1] + string[2]) % 256 == string[3])):
                        flag = [3]
                        break
                    else:
                        # ожидаем окончания приема, когда длина массива равна (длине оргумента) + 5
                        # и совпадает контрольная сумма агрумента
                        if len(string) == (string[2] + 5):
                            if sum(string[4:-1]) % 256 == string[-1]:
                                break
                            else:
                                flag = [4]
                                break
                        elif len(string) > (string[2] + 5):
                            flag = [5]
                            break

            if time.time() - tic > timeout:
                flag = [1]
                break

        result = string + flag
        self._waiting_for_response = False

        return result

    @staticmethod
    def _int_to_2s(A):
        # перевод знакового целого в список из двух байт
        return list(A.to_bytes(2, byteorder='little', signed=True))

    @staticmethod
    def _i2s_to_int(A):
        # перевод двух байт в знаковое целое
        return int.from_bytes(bytes(A), byteorder='little', signed=True)

    @staticmethod
    def _lookup_SBGC32(self):

        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/ttyUSB*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = ''
        for port in ports:
            try:
                s = serial.Serial(port, baudrate=115200)
                # пытаемся получить информацию о плате управления по текущему порту
                self._send_command(s)
                response = self._get_response(s)

                # если найдена плата с версией 3.0 и выше, то с ней будем работать
                if (response[-1]) == 0 and (response[4] >= 30):
                    print('Плата управления найдена на '+port)
                    print('Board_ver = ', format(response[4]/10, '.2f'))
                    firmware_ver = response[5] + response[6] * 256
                    print('Firmware_ver ='+str(int(firmware_ver/1000))+'.'+str(int((firmware_ver % 1000)/10))+'b'
                          + str(firmware_ver % 10))
                    print('STATE_FLAGS1 =', bin(response[7]+256)[3:11])
                    print('BOARD_FEATURES =', bin(response[9]+256)[3:11], bin(response[8]+256)[3:11])
                    print('CONNECTION_FLAG =', bin(response[10] + 256)[3:11])
                    print('FRW_EXTRA_ID =', response[11:14])
                    print('RESERVED =', response[15:21])
                    print()
                    time.sleep(1)

                    #self._write_params(s)

                    result = port
                    break

                s.close()

            except (OSError, serial.SerialException):
                pass

        return result

    def _write_params(self, s):
        #TODO
        # принудительная запись параметров в плату управления
        # стоит делать, если точно будут получены PID-ы и прочие переметры

        # для отладки чтение параметров
        self._send_command(s, CMD_READ_PARAMS_3)

        response = self._get_response(s, 1)

        if (response[-1]) == 0 and (response[1] == CMD_READ_PARAMS_3):
            # разбираем ответ
            # profile_id  = response[4]
            # roll_P      = response[5]
            # roll_I      = response[6]
            # roll_D      = response[7]
            # roll_POWER  = response[8]
            # roll_INVERT = response[9]
            # roll_POLES  = response[10]
            # pitch_P      = response[11]
            # pitch_I      = response[12]
            # pitch_D      = response[13]
            # pitch_POWER  = response[14]
            # pitch_INVERT = response[15]
            # pitch_POLES  = response[16]
            # yaw_P      = response[17]
            # yaw_I      = response[18]
            # yaw_D      = response[19]
            # yaw_POWER  = response[20]
            # yaw_INVERT = response[21]
            # yaw_POLES  = response[22]

            ACC_LIMITER_ALL = response[23]

            # EXT_FC_GAIN_1 = int.from_bytes(bytes(response[24]), byteorder='little', signed=True)
            # EXT_FC_GAIN_2 = int.from_bytes(bytes(response[25]), byteorder='little', signed=True)

            roll_RC_MIN_ANGLE = self._i2s_to_int(response[26:28])
            roll_RC_MAX_ANGLE = self._i2s_to_int(response[28:30])

            # roll_RC_MODE_bit0 = (response[30] & (1<<0)) >> 0
            # roll_RC_MODE_bit1 = (response[30] & (1 << 1)) >> 1
            # roll_RC_MODE_bit2 = (response[30] & (1 << 2)) >> 2
            # roll_RC_MODE_bit3 = (response[30] & (1 << 2)) >> 3

            roll_RC_LPF = response[31]
            roll_RC_SPEED = response[32]
            roll_RC_FOLLOW = int.from_bytes(bytes(response[33]), byteorder='little', signed=True)

            pitch_RC_MIN_ANGLE = self._i2s_to_int(response[34:36])
            pitch_RC_MAX_ANGLE = self._i2s_to_int(response[36:38])

            # pitch_RC_MODE_bit0 = (response[38] & (1 << 0)) >> 0
            # pitch_RC_MODE_bit1 = (response[38] & (1 << 1)) >> 1
            # pitch_RC_MODE_bit2 = (response[38] & (1 << 2)) >> 2
            # pitch_RC_MODE_bit3 = (response[38] & (1 << 2)) >> 3

            # pitch_RC_LPF = response[39]
            # pitch_RC_SPEED = response[40]
            # pitch_RC_FOLLOW = int.from_bytes(bytes(response[41]), byteorder='little', signed=True)

            yaw_RC_MIN_ANGLE = self._i2s_to_int(response[42:44])
            yaw_RC_MAX_ANGLE = self._i2s_to_int(response[44:46])

            # yaw_RC_MODE_bit0 = (response[46] & (1 << 0)) >> 0
            # yaw_RC_MODE_bit1 = (response[46] & (1 << 1)) >> 1
            # yaw_RC_MODE_bit2 = (response[46] & (1 << 2)) >> 2
            # yaw_RC_MODE_bit3 = (response[46] & (1 << 2)) >> 3

            yaw_RC_LPF = response[47]
            yaw_RC_SPEED = response[48]
            yaw_RC_FOLLOW = int.from_bytes(bytes(response[49]), byteorder='little', signed=True)

            # GYRO_TRUST = response[50]
            # USE_MODEL = response[51]
            # PWM_FREQ = response[52]

            # SERIAL_SPEED = response[53]
            # RC_TRIM_1 = int.from_bytes(bytes(response[54]), byteorder='little', signed=True)
            # RC_TRIM_2 = int.from_bytes(bytes(response[55]), byteorder='little', signed=True)
            # RC_TRIM_3 = int.from_bytes(bytes(response[56]), byteorder='little', signed=True)
            # RC_DEADBAND = response[57]
            # RC_EXPO_RATE = response[58]
            # RC_VIRT_MODE = response[59]
            # RC_MAP_ROLL = response[60]
            # RC_MAP_PITCH = response[61]
            # RC_MAP_YAW = response[62]
            # RC_MAP_CMD = response[63]
            # RC_MAP_FC_ROLL = response[64]
            # RC_MAP_FC_PITCH = response[65]

            RC_MIX_FC_ROLL = response[66]
            RC_MIX_FC_PITCH = response[67]

            FOLLOW_MODE = response[68]

            FOLLOW_DEADBAND = response[69]
            FOLLOW_EXPO_RATE = response[70]
            FOLLOW_OFFSET_1 = int.from_bytes(bytes(response[71]), byteorder='little', signed=True)
            FOLLOW_OFFSET_2 = int.from_bytes(bytes(response[72]), byteorder='little', signed=True)
            FOLLOW_OFFSET_3 = int.from_bytes(bytes(response[73]), byteorder='little', signed=True)

            # AXIS_TOP = int.from_bytes(bytes(response[74]), byteorder='little', signed=True)
            # AXIS_RIGHT = int.from_bytes(bytes(response[75]), byteorder='little', signed=True)
            # FRAME_AXIS_TOP = int.from_bytes(bytes(response[76]), byteorder='little', signed=True)
            # FRAME_AXIS_RIGHT = int.from_bytes(bytes(response[77]), byteorder='little', signed=True)

            # FRAME_IMU_POS = response[78]

            # GYRO_DEADBAND = response[79]

            # GYRO_SENS = response[80]

            # I2C_SPEED_FAST = response[81]
            # SKIP_GYRO_CALIB = response[82]

            # RC_CMD_LOW = response[83]
            # RC_CMD_MID = response[84]
            # RC_CMD_HIGH = response[85]

            # MENU_CMD_1 = response[86]
            # MENU_CMD_2 = response[87]
            # MENU_CMD_3 = response[88]
            # MENU_CMD_4 = response[89]
            # MENU_CMD_5 = response[90]
            # MENU_CMD_LONG = response[91]

            # MOTOR_OUTPUT_1 = response[92]
            # MOTOR_OUTPUT_2 = response[93]
            # MOTOR_OUTPUT_3 = response[94]

            # BAT_THRESHOLD_ALARM = self._i2s_to_int(response[95:97])
            # BAT_THRESHOLD_MOTORS = self._i2s_to_int(response[97:99])
            # BAT_COMP_REF = self._i2s_to_int(response[99:101])

            # BEEPER_MODES = response[101]

            FOLLOW_ROLL_MIX_START = response[102]
            FOLLOW_ROLL_MIX_RANGE = response[103]

            BOOSTER_POWER_1 = response[104]
            BOOSTER_POWER_2 = response[105]
            BOOSTER_POWER_3 = response[106]

            FOLLOW_SPEED_1 = response[107]
            FOLLOW_SPEED_2 = response[108]
            FOLLOW_SPEED_3 = response[109]

            # RFAME_ANGLE_FROM_MOTORS = response[110]

            # RC_MEMORY_1 = self._i2s_to_int(response[111:113])
            # RC_MEMORY_2 = self._i2s_to_int(response[113:115])
            # RC_MEMORY_3 = self._i2s_to_int(response[115:117])

            # SERVO1_OUT = response[117]
            # SERVO2_OUT = response[118]
            # SERVO3_OUT = response[119]
            # SERVO4_OUT = response[120]

            # SERVO_RATE = response[121]

            # ADAPTIVE_PID_ENABLED = response[122]
            # ADAPTIVE_PID_TRESHOLD = response[123]
            # ADAPTIVE_PID_RATE = response[124]
            # ADAPTIVE_PID_RECOVERY_FACTOR = response[125]

            FOLLOW_LPF_1 = response[126]
            FOLLOW_LPF_2 = response[127]
            FOLLOW_LPF_3 = response[128]

            # GENERAL_FLAGS1 = response[129]+response[130]<<8
            # PROFILE_FLAGS1 = response[131] + response[132] << 8
            # SPEKTRUM_MODE = response[133]
            # ORDER_OF_AXES = response[134]
            # EULER_ORDER = response[135]
            # CUR_IMU = response[136]
            # CUR_PROFILE_ID = response[137]

            print('PARAMS: ', response)

            # print('profile_id = ',response[4])
            # print('roll_P = ',response[5])
            # print('roll_I = ',response[6])
            # print('roll_D = ',response[7])
            # print('roll_POWER = ',response[8])
            # print('roll_INVERT = ',response[9])
            # print('roll_POLES = ',response[10])
            # print('pitch_P = ',response[11])
            # print('pitch_I = ',response[12])
            # print('pitch_D = ',response[13])
            # print('pitch_POWER = ',response[14])
            # print('pitch_INVERT = ',response[15])
            # print('pitch_POLES = ',response[16])
            # print('yaw_P = ',response[17])
            # print('yaw_I = ',response[18])
            # print('yaw_D = ',response[19])
            # print('yaw_POWER = ',response[20])
            # print('yaw_INVERT = ',response[21])
            # print('yaw_POLES = ',response[22])

            print('ACC_LIMITER_ALL = ', response[23])

            # print('EXT_FC_GAIN_1 = ',int.from_bytes(bytes(response[24]), byteorder='little', signed=True))
            # print('EXT_FC_GAIN_2 = ',int.from_bytes(bytes(response[25]), byteorder='little', signed=True))

            print('roll_RC_MIN_ANGLE = ', self._i2s_to_int(response[26:28]))
            print('roll_RC_MAX_ANGLE = ', self._i2s_to_int(response[28:30]))

            # print('roll_RC_MODE_bit0 = ',(response[30] & (1 << 0)) >> 0)
            # print('roll_RC_MODE_bit1 = ',(response[30] & (1 << 1)) >> 1)
            # print('roll_RC_MODE_bit2 = ',(response[30] & (1 << 2)) >> 2)
            # print('roll_RC_MODE_bit3 = ',(response[30] & (1 << 2)) >> 3)

            print('roll_RC_LPF = ', response[31])
            print('roll_RC_SPEED = ', response[32])
            print('roll_RC_FOLLOW = ', int.from_bytes(bytes(response[33]), byteorder='little', signed=True))

            print('pitch_RC_MIN_ANGLE = ', self._i2s_to_int(response[34:36]))
            print('pitch_RC_MAX_ANGLE = ', self._i2s_to_int(response[36:38]))

            # print('pitch_RC_MODE_bit0 = ',(response[38] & (1 << 0)) >> 0)
            # print('pitch_RC_MODE_bit1 = ',(response[38] & (1 << 1)) >> 1)
            # print('pitch_RC_MODE_bit2 = ',(response[38] & (1 << 2)) >> 2)
            # print('pitch_RC_MODE_bit3 = ',(response[38] & (1 << 2)) >> 3)

            print('pitch_RC_LPF = ', response[39])
            print('pitch_RC_SPEED = ', response[40])
            print('pitch_RC_FOLLOW = ', int.from_bytes(bytes(response[41]), byteorder='little', signed=True))

            print('yaw_RC_MIN_ANGLE = ', self._i2s_to_int(response[42:44]))
            print('yaw_RC_MAX_ANGLE = ', self._i2s_to_int(response[44:46]))

            # print('yaw_RC_MODE_bit0 = ',(response[46] & (1 << 0)) >> 0)
            # print('yaw_RC_MODE_bit1 = ',(response[46] & (1 << 1)) >> 1)
            # print('yaw_RC_MODE_bit2 = ',(response[46] & (1 << 2)) >> 2)
            # print('yaw_RC_MODE_bit3 = ',(response[46] & (1 << 2)) >> 3)

            print('yaw_RC_LPF = ', response[47])
            print('yaw_RC_SPEED = ', response[48])
            print('yaw_RC_FOLLOW = ', int.from_bytes(bytes(response[49]), byteorder='little', signed=True))

            # print('GYRO_TRUST = ',response[50])
            # print('USE_MODEL = ',response[51])
            # print('PWM_FREQ = ',response[52])

            # print('SERIAL_SPEED = ',response[53])
            # print('RC_TRIM_1 = ',int.from_bytes(bytes(response[54]), byteorder='little', signed=True))
            # print('RC_TRIM_2 = ',int.from_bytes(bytes(response[55]), byteorder='little', signed=True))
            # print('RC_TRIM_3 = ',int.from_bytes(bytes(response[56]), byteorder='little', signed=True))
            # print('RC_DEADBAND = ',response[57])
            # print('RC_EXPO_RATE = ',response[58])
            # print('RC_VIRT_MODE = ',response[59])
            # print('RC_MAP_ROLL = ',response[60])
            # print('RC_MAP_PITCH = ',response[61])
            # print('RC_MAP_YAW = ',response[62])
            # print('RC_MAP_CMD = ',response[63])
            # print('RC_MAP_FC_ROLL = ',response[64])
            # print('RC_MAP_FC_PITCH = ',response[65])

            # print('RC_MIX_FC_ROLL = ',response[66])
            # print('RC_MIX_FC_PITCH = ',response[67])

            print('FOLLOW_MODE = ', response[68])

            print('FOLLOW_DEADBAND = ', response[69])
            print('FOLLOW_EXPO_RATE = ', response[70])
            print('FOLLOW_OFFSET_1 = ', int.from_bytes(bytes(response[71]), byteorder='little', signed=True))
            print('FOLLOW_OFFSET_2 = ', int.from_bytes(bytes(response[72]), byteorder='little', signed=True))
            print('FOLLOW_OFFSET_3 = ', int.from_bytes(bytes(response[73]), byteorder='little', signed=True))

            # print('AXIS_TOP = ',int.from_bytes(bytes(response[74]), byteorder='little', signed=True))
            # print('AXIS_RIGHT = ',int.from_bytes(bytes(response[75]), byteorder='little', signed=True))
            # print('FRAME_AXIS_TOP = ',int.from_bytes(bytes(response[76]), byteorder='little', signed=True))
            # print('FRAME_AXIS_RIGHT = ',int.from_bytes(bytes(response[77]), byteorder='little', signed=True))

            # print('FRAME_IMU_POS = ',response[78])

            # print('GYRO_DEADBAND = ',response[79])

            # print('GYRO_SENS = ',response[80])

            # print('I2C_SPEED_FAST = ',response[81])
            # print('SKIP_GYRO_CALIB = ',response[82])

            # print('RC_CMD_LOW = ',response[83])
            # print('RC_CMD_MID = ',response[84])
            # print('RC_CMD_HIGH = ',response[85])

            # print('MENU_CMD_1 = ',response[86])
            # print('MENU_CMD_2 = ',response[87])
            # print('MENU_CMD_3 = ',response[88])
            # print('MENU_CMD_4 = ',response[89])
            # print('MENU_CMD_5 = ',response[90])
            # print('MENU_CMD_LONG = ',response[91])

            # print('MOTOR_OUTPUT_1 = ',response[92])
            # print('MOTOR_OUTPUT_2 = ',response[93])
            # print('MOTOR_OUTPUT_3 = ',response[94])

            # print('BAT_THRESHOLD_ALARM = ',self._i2s_to_int(response[95:97]))
            # print('BAT_THRESHOLD_MOTORS = ',self._i2s_to_int(response[97:99]))
            # print('BAT_COMP_REF = ',self._i2s_to_int(response[99:101]))

            # print('BEEPER_MODES = ',response[101])

            print('FOLLOW_ROLL_MIX_START = ', response[102])
            print('FOLLOW_ROLL_MIX_RANGE = ', response[103])

            # print('BOOSTER_POWER_1 = ',response[104])
            # print('BOOSTER_POWER_2 = ',response[105])
            # print('BOOSTER_POWER_3 = ',response[106])

            print('FOLLOW_SPEED_1 = ', response[107])
            print('FOLLOW_SPEED_2 = ', response[108])
            print('FOLLOW_SPEED_3 = ', response[109])

            # print('RFAME_ANGLE_FROM_MOTORS = ',response[110])

            # print('RC_MEMORY_1 = ',self._i2s_to_int(response[111:113]))
            # print('RC_MEMORY_2 = ',self._i2s_to_int(response[113:115]))
            # print('RC_MEMORY_3 = ',self._i2s_to_int(response[115:117]))

            # print('SERVO1_OUT = ',response[117])
            # print('SERVO2_OUT = ',response[118])
            # print('SERVO3_OUT = ',response[119])
            # print('SERVO4_OUT = ',response[120])

            # print('SERVO_RATE = ',response[121])

            # print('ADAPTIVE_PID_ENABLED = ',response[122])
            # print('ADAPTIVE_PID_TRESHOLD = ',response[123])
            # print('ADAPTIVE_PID_RATE = ',response[124])
            # print('ADAPTIVE_PID_RECOVERY_FACTOR = ',response[125])

            print('FOLLOW_LPF_1 = ', response[126])
            print('FOLLOW_LPF_2 = ', response[127])
            print('FOLLOW_LPF_3 = ', response[128])

            # print('GENERAL_FLAGS1 = ',response[129] + response[130] << 8)
            # print('PROFILE_FLAGS1 = ',response[131] + response[132] << 8)
            # print('SPEKTRUM_MODE = ',response[133])
            # print('ORDER_OF_AXES = ',response[134])
            # print('EULER_ORDER = ',response[135])
            # print('CUR_IMU = ',response[136])
            # print('CUR_PROFILE_ID = ',response[137])
